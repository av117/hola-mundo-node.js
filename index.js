const http = require('http');
const webPort = 8080

// Name
let nameUser = 'Allan'

http.createServer((req, res) => {
        res.write('Hola Mundo');
        res.write(`\nBienvenido ${nameUser}`);
        res.end();
    })
    .listen(webPort)

console.log(`Escuchando el puerto, ${webPort}`)